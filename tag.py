AWSTemplateFormatVersion: '2010-09-09'
Description: 'CFT for SSM Document with State Manager Association'
Parameters:
  TagKey:
    Type: String
    Description: The key of the tag to target instances

  TagValue:
    Type: String
    Description: The value of the tag to target instances

Resources:
  ColdfusionInstallPackages:
    Type: AWS::SSM::Document
    Properties:
     DocumentType: "Command"
     Content:
        description: cfusion poc document
        schemaVersion: '2.2'
        mainSteps:
          - name: cfusion_poc
            action: "aws:runShellScript"
            inputs:
                runCommand:
                  - echo "`hostname -I`    `hostname`   `hostname -s`" >> /etc/hosts
                  - if ! rpm -q java-11-openjdk; then sudo dnf install java-11-openjdk -y; fi
                  - if ! rpm -q yajl; then sudo dnf -y install yajl.x86_64; fi
                  - if ! rpm -q telnet; then sudo dnf -y install telnet.x86_64; fi
                  - if ! rpm -q unzip; then sudo dnf -y install unzip.x86_64; fi
                  - if ! rpm -q postfix; then sudo dnf -y install postfix.x86_64; fi
                  - if ! rpm -q zip; then sudo dnf -y install zip unzip; fi
                  - echo "net.ipv4.conf.default.accept_redirects = 0" | sudo tee -a /etc/sysctl.conf
                  - echo "net.ipv4.conf.default.secure_redirects = 0" | sudo tee -a /etc/sysctl.conf
                  - echo "net.ipv4.conf.all.secure_redirects = 0" | sudo tee -a /etc/sysctl.conf
                  - echo "net.ipv4.conf.all.accept_redirects = 0" | sudo tee -a /etc/sysctl.conf
                  - echo "kernel.shmall = 4294967296" | sudo tee -a /etc/sysctl.conf
                  - echo "cfusion    soft   nofile   10240" | sudo tee -a /etc/security/limits.conf
                  - echo "cfusion    hard   nofile   10240" | sudo tee -a /etc/security/limits.conf
                  #- sudo groupadd -g 668 localgroup; sudo groupadd -g 1248 wasgroup; sudo groupadd -g 65537 erasadm; sudo groupadd -g 219 tomcat; sudo groupadd -g 710 cfusion; sudo groupadd -g 1250 splunk; sudo groupadd -g 7789 LTIM_LinuxCXP
                  #- sudo useradd -u 1221 -g 1248 -G "erasadm,tomcat"  -m -d /export/home/wasuser -p '!!' -c "wasuser user" -s /bin/bash wasuser; sudo useradd -u 710 -g 710 -G "cfusion,wasgroup"   -m -d /export/home/cfusion -p '!!' -c "Coldfusion User" -s /bin/bash cfusion; sudo useradd -u 1250 -g 1250 -m -d /opt/splunk -p '!!' -c "Splunk User" -s /bin/bash splunk; sudo useradd -u 219 -g 219   -m -d /opt/tomcatX -p 'x' -c "Tomcat User" -s /bin/bash tomcat; sudo useradd -u 7789 -g 7789 -m -d /export/home/LTIM_LinuxCXP -p '$1$WQxPVLhi$mGRvpz2EokwLKbaokC/t51' -c "CloudXperienz User" -s /bin/bash LTIM_LinuxCXP
                  - if ! command -v aws > /dev/null; then curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"; unzip awscliv2.zip; sudo ./aws/install; fi
                  - if [ ! -d /opt/filestore/appdata/amcas/schooldata ]; then sudo mkdir -p /opt/filestore/appdata/amcas/schooldata; fi
                  - if [ ! -d /opt/filestore/coldfusion ]; then sudo mkdir -p /opt/filestore/coldfusion; fi
                  - if [ ! -d /opt/filestore/appdata/amcas/transcripts ]; then sudo mkdir -p /opt/filestore/appdata/amcas/transcripts; fi
                  - if [ ! -d /opt/filestore/shared/AMCASPermanentStorage/VerificationManual ]; then sudo mkdir -p /opt/filestore/shared/AMCASPermanentStorage/VerificationManual; fi
                  - if [ ! -d /opt/filestore/appdata/eras/nfs/ftphome ]; then sudo mkdir -p /opt/filestore/appdata/eras/nfs/ftphome; fi
                  - if [ ! -d /etc/aamc ]; then sudo mkdir /etc/aamc; fi
                  - chmod 755 /etc/aamc
                  - sed -i 's/^SELINUX=.*/SELINUX=permissive/g' /etc/selinux/config
                  - timedatectl set-timezone America/New_York
                  - sudo chown root:root /opt/filestore; sudo chmod 0755 /opt/filestore/; sudo chown cfusion:cfusion /opt/filestore/coldfusion; sudo chmod 0755 /opt/filestore/coldfusion; sudo chown wasuser:wasgroup /opt/filestore/appdata; sudo chmod 0777 /opt/filestore/appdata; sudo chown wasuser:wasgroup /opt/filestore/shared; sudo chmod 0777 /opt/filestore/shared; sudo chown tomcat:tomcat  /opt/filestore/appdata/amcas; sudo chmod 0755 /opt/filestore/appdata/amcas; sudo chown tomcat:tomcat  /opt/filestore/appdata/amcas/schooldata; sudo chmod 0777 /opt/filestore/appdata/amcas/schooldata; sudo chown root:root /opt/filestore/appdata/amcas/transcripts; sudo chmod 0777 /opt/filestore/appdata/amcas/transcripts; sudo chown tomcat:tomcat /opt/filestore/appdata/eras; sudo chmod 0777 /opt/filestore/appdata/eras
                  - postconf -e "myhostname = `hostname`"
                  - postconf -e "mydomain = `hostname -d`"
                  - postconf -e "myorigin = \$myhostname"
                  - postconf -e "mynetworks = 127.0.0.0/8 0.0.0.0/0"
                  - postconf -e "relay_domains = \$mydestination"
                  - postconf -e "relayhost = dv-u-utilities-mtrap-01.dv.aamc.org"
                  - postconf -e "mailbox_size_limit = 524288000"
                  - postconf -e "smtpd_banner = \$myhostname ESMTP \$mail_name"
                  - postconf -e "luser_relay ="
                  - postconf -e "transport_maps ="
                  - systemctl enable postfix
                  - systemctl start postfix
                  - sudo dnf -y install  https://dl.fedoraproject.org/pub/epel/9/Everything/x86_64/Packages/c/collectd-5.12.0-24.el9.x86_64.rpm
                  - sudo dnf -y install https://dl.fedoraproject.org/pub/epel/9/Everything/x86_64/Packages/c/collectd-write_http-5.12.0-24.el9.x86_64.rpm
                  - if [ ! -f /tmp/ColdFusion_2023_GUI_WWEJ_linux64.bin ]; then aws s3 cp s3://aamc-coldfusion-legacy-dev/ColdFusion_2023_GUI_WWEJ_linux64.bin /tmp/; fi
                  - if [ ! -f /root/silent.properties ]; then aws s3 cp s3://coldfusion-config-development/appconfig/silent.properties /root/; fi
                  - if [ ! -d /opt/coldfusion2023/cfusion/common ]; then sudo mkdir -p /opt/coldfusion2023/cfusion/common; fi
                  - aws s3 sync s3://aamc-coldfusion-legacy-dev/common /opt/coldfusion2023/cfusion/common
                  - chown -R cfusion:bin /opt/coldfusion2023/cfusion/common 
                  - aws s3 cp s3://aamc-coldfusion-legacy-dev/coldfusion_2023.txt /etc/init.d
                  - mv /etc/init.d/coldfusion_2023.txt /etc/init.d/coldfusion_2023
                  - sudo chmod +x /etc/init.d/coldfusion_2023
                  - aws s3 cp s3://aamc-coldfusion-legacy-dev/jvm.config /opt/coldfusion2023/cfusion/bin/
                  - chown   cfusion:bin   /opt/coldfusion2023/cfusion/bin/jvm.config
                  - sudo chmod +x /tmp/ColdFusion_2023_GUI_WWEJ_linux64.bin
                  - cd /tmp/; ./ColdFusion_2023_GUI_WWEJ_linux64.bin -f /root/silent.properties
                  - systemctl enable collectd
                  - systemctl start collectd
                  - aws s3 cp s3://aamc-coldfusion-legacy-dev/neo-security.xml /opt/coldfusion2023/cfusion/lib
                  - chown  cfusion:bin    /opt/coldfusion2023/cfusion/lib/neo-security.xml
                  - aws s3 cp s3://aamc-coldfusion-legacy-dev/adminconfig.xml /opt/coldfusion2023/cfusion/lib
                  - chown  cfusion:bin    /opt/coldfusion2023/cfusion/lib/adminconfig.xml
                  #- /etc/init.d/coldfusion_2023  start


  ColdfusionInstallPackagesAssociation:
    Type: 'AWS::SSM::Association'
    DependsOn : ColdfusionInstallPackages
    Properties:
      Name: !Ref ColdfusionInstallPackages
      AssociationName: 'ColdfusionInstallPackages'
      Targets:
        - Key: !Sub 'tag:${TagKey}'
          Values:
            - !Ref TagValue